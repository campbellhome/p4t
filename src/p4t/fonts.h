// Copyright (c) 2012-2018 Matt Campbell
// MIT license (see License.txt)

#pragma once

void Fonts_MarkAtlasForRebuild();
bool Fonts_UpdateAtlas();
void Fonts_Menu();
